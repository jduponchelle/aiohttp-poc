# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 GNS3 Technologies Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Set up and run the server.
"""

import asyncio
from aiohttp import web

from demoserver.handlers import *
from demoserver.route import Route

import logging
log = logging.getLogger(__name__)


class Server(object):
    def __init__(self, host = "127.0.0.1", port = "8000"):
        self._host = host
        self._port = port

    def run(self):
        """
        Starts the web server
        """

        self.loop = asyncio.get_event_loop()
        app = web.Application(loop = self.loop)
        for method, route, handler in Route.get_routes():
            app.router.add_route(method, route, handler)
        log.info("Starting server on {}:{}".format(self._host, self._port))

        self.loop.run_until_complete(self._loop_application(app))
        try:
            self.loop.run_forever()
        except (KeyboardInterrupt, SystemExit):
            log.info("\nExiting...")

    def _loop_application(self, app):
        srv = yield from self.loop.create_server(app.make_handler(),
                                        self._host, self._port)
        return srv
