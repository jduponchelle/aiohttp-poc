# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 GNS3 Technologies Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from ..route import Route
from ..schema import VERSION_SCHEMA
from ..version import __version__
import asyncio

class VpcsHandler(object):
    @classmethod
    @Route.get(
        r"/version",
        description="Retrieve server version number",
        output = VERSION_SCHEMA)
    def version(request):
        return {'version': __version__}

    @classmethod
    @Route.get(r"/sleep")
    def subprocess(request):
        proc = yield from asyncio.create_subprocess_shell("sleep 10")
        exitcode = yield from proc.wait()
        print(exitcode)
        return {'exit': exitcode}
