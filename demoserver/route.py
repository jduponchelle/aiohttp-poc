# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 GNS3 Technologies Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import jsonschema
from aiohttp import web
import asyncio

class Route(object):
    '''Decorator adding:
        * jsonschema verification,
        * routing inside handlers
        * documentation informations about endpoints
    '''

    _routes = []
    _documentation = {}

    @classmethod
    def get(self, path, *args, **kw):
        return self._route('GET', path, *args, **kw)

    @classmethod
    def post(self, path, *args, **kw):
        return self._route('POST', path, *args, **kw)

    @classmethod
    def put(self, path, *args, **kw):
        return self._route('PUT', path, *args, **kw)

    @classmethod
    def _route(self, method, path, *args, **kw):
        #TODO: move to a middleware JSON decoding?

        #This block is executed only the first time
        output_schema = kw.get("output", {})
        input_schema = kw.get("input", {})
        self._path = path
        self._documentation.setdefault(self._path, {"methods": []})
        def register(func):
            route = self._path

            self._documentation[route]["methods"].append({
                "method": method,
                "status_codes": kw.get("status_codes", {200: "OK"}),
                "parameters": kw.get("parameters", {}),
                "output_schema": output_schema,
                "input_schema": input_schema,
                "description": kw.get("description", "")
            })
            func = asyncio.coroutine(func)

            @asyncio.coroutine
            def control_schema(request):
                #This block is executed at each method call
                if request.content_length is not None and request.content_length > 0:
                    #TODO: raise 400 if invalid JSON
                    body = yield from request.read()
                    body = json.loads(body.decode('utf-8'))
                    request.json = body

                jsonschema.validate(request.json, input_schema)
                #TODO: Catch schema validation

                answer = yield from func(request)
                jsonschema.validate(answer, output_schema)

                #TODO: Only on debug/test mode
                headers = {'X-Route': route}

                return web.Response(body=json.dumps(answer).encode('utf-8'), headers = headers)
            self._routes.append((method, self._path, control_schema, ))

            return control_schema
        return register

    @classmethod
    def get_routes(self):
        return self._routes

    @classmethod
    def get_documentation(self):
        return self._documentation

