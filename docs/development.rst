Development
############

Documentation
==============

Build doc
----------
In the project root folder:

.. code-block:: bash
    
    ./documentation.sh

The output is available inside *docs/_build/html*

Tests
======

Run tests
----------

.. code-block:: bash
    
    py.test -v

