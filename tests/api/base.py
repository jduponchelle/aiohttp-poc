# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 GNS3 Technologies Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Base code use for all API tests"""
import json
import re
import asyncio
import unittest
from aiohttp import web
import aiohttp
from demoserver.route import Route
from demoserver.handlers import *

class ApiTestCase(unittest.TestCase):
    _host = '127.0.0.1'
    _port = "8001"

    def setUp(self):
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(None) #Turn off main loop to avoid conflict between tests
        self.app = web.Application(loop = self.loop)
        for method, route, handler in Route.get_routes():
             self.app.router.add_route(method, route, handler)
        srv = self.loop.create_server(self.app.make_handler(), self._host, self._port)
        self.loop.run_until_complete(srv)

    def tearDown(self):
        self.loop.close()

    def post(self, path, body, **kwargs):
        return self._fetch("POST", path, body, **kwargs)

    def get(self, path, **kwargs):
        return self._fetch("GET", path, **kwargs)

    def get_url(self, path):
        return "http://{}:{}{}".format(self._host, self._port, path)

    def _fetch(self, method, path, body = None, **kwargs):
        """Fetch an url, parse the JSON and return response

        Options: example if True the session is included inside documentation
        """
        if body is not None:
            body = json.dumps(body, sort_keys = True, indent = 4)

        @asyncio.coroutine
        def go(future):
            response = yield from aiohttp.request(method, self.get_url(path), data = body, loop = self.loop)
            future.set_result(response)
        future = asyncio.Future(loop = self.loop)
        asyncio.async(go(future), loop = self.loop)
        self.loop.run_until_complete(future)
        response = future.result()

        @asyncio.coroutine
        def go(future, response):
            response = yield from response.read()
            future.set_result(response)
        future = asyncio.Future(loop = self.loop)
        asyncio.async(go(future, response), loop = self.loop)
        self.loop.run_until_complete(future)
        response.body = future.result()
        response.route = response.headers['X-Route']

        if response.body is not None:
            response.json = json.loads(response.body.decode("utf-8"))
        else:
            response.json = {}
        if kwargs.get('example') == True:
            self._dump_example(method, response.route, body, response)
        return response

    def _dump_example(self, method, path, body, response):
        """Dump the request for the documentation"""
        with open(self._example_file_path(method, path), 'w+') as f:
            f.write("{} {} HTTP/1.1\n".format(method, path))
            if body:
                f.write(body)
            f.write("\n\n\n")
            f.write("HTTP/1.1 {}\n".format(response.status))
            for header, value in sorted(response.headers.items()):
                f.write("{}: {}\n".format(header, value))
            f.write("\n")
            f.write(json.dumps(json.loads(response.body.decode('utf-8')), sort_keys = True, indent = 4))
            f.write("\n")

    def _example_file_path(self, method, path):
        path = re.sub('[^a-z0-9]', '', path)
        return "docs/api/examples/{}_{}.txt".format(method.lower(), path)

